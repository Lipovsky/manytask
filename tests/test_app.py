import pytest

import shadts

@pytest.fixture
def test_app():
    shadts.app.config.from_pyfile("../env-local.cfg")
    return shadts.app
