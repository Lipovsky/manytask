#!/bin/sh

exec env \
  SHADTS_SETTINGS=/etc/manytask/manytask.conf \
  PYTHONPATH=. \
  FLASK_APP=shadts \
  FLASK_DEBUG=1 \
  flask run --host='0.0.0.0'
