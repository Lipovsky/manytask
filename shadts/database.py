import redis

from . import app

def _decode_dict_utf8(d):
    decoded = {}
    for k, v in d.items():
        decoded[k.decode('utf8')] = v.decode('utf8')
    return decoded

class UserInfo:
    def __init__(self, user, first, last, group):
        self.username = user
        self.firstname = first
        self.lastname = last
        self.groupname = group

class CourseDatabase:
    def __init__(self, db_addr, course):
        self.client = redis.StrictRedis(db_addr)
        self.course = course

    @staticmethod
    def from_config():
        return CourseDatabase(
            db_addr=app.config["REDIS"],
            course=app.config["COURSE_NAME"])

    def _hget_int(self, hash, key):
        value = self.client.hget(hash, key)
        if not value:
            return 0
        return int(value.decode('utf8').split('.')[0])

    def _task_score_hash(self, user):
        return "task_score:{}:{}".format(self.course, user)

    def update_score(self, user, task, score):
        score = int(score)

        task_score_hash = self._task_score_hash(user)

        prev_score = self._hget_int(task_score_hash, task)

        new_score = max(prev_score, score)
        self.client.hset(task_score_hash, task, new_score)

    def get_score(self, user, task):
        task_score_hash = self._task_score_hash(user)
        value = self.client.hget(task_score_hash, task)
        if value is None:
            return None
        return int(value)

    def get_user_scores(self, user, text=False):
        task_scores = self.client.hgetall(
            "task_score:{}:{}".format(self.course, user))

        task_scores = _decode_dict_utf8(task_scores)

        if text:
            return task_scores

        # convert scores to integers
        for task_name in task_scores:
            task_scores[task_name] = int(task_scores[task_name])

        return task_scores


    def _save_user_attr(self, user, attr, value):
        self.client.hset("user_info:{}:{}".format(self.course, user), attr, value)

    def _get_user_attr(self, user, attr):
        value = self.client.hget("user_info:{}:{}".format(self.course, user), attr)
        if value:
            return value.decode("utf-8")
        else:
            return None

    def save_user(self, username, firstname, lastname, groupname):
        self._save_user_attr(username, "first_name", firstname.lower())
        self._save_user_attr(username, "last_name", lastname.lower())
        self._save_user_attr(username, "group", groupname.lower())

    def has_user(self, user):
        return self._get_user_attr(user, "first_name") is not None

    def get_user_info(self, user):
        firstname = self._get_user_attr(user, "first_name")
        lastname  = self._get_user_attr(user, "last_name")
        groupname = self._get_user_attr(user, "group")

        if firstname is None or lastname is None or groupname is None:
            raise ValueError('Unregistered user {}: try register first before login'.format(user))

        return UserInfo(user, firstname, lastname, groupname)

    def list_users(self):
        users = []

        for user_info_hash in self.client.scan_iter('user_info:{}:*'.format(self.course)):
            username = user_info_hash.decode('utf-8').split(':', 2)[2]
            try:
                user_info = self.get_user_info(username)
            except ValueError as e:
                continue
            users.append(user_info)

        return users

def open():
    return CourseDatabase.from_config()

