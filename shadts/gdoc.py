import json
import gspread

from google.oauth2.service_account import Credentials

from . import app
from . import gitlab_repos

class RatingsTable:
    @classmethod
    def from_config(cls):
        return RatingsTable(
            cls.credentials(app.config["GDOC_ACCOUNT_FILE"]),
            app.config["GDOC_SPREADSHEET"])

    @staticmethod
    def credentials(account_file):
        scope = ['https://spreadsheets.google.com/feeds']
        return Credentials.from_service_account_file(account_file, scopes=scope)

    def __init__(self, credentials, sheet_id):
        gs = gspread.authorize(credentials)
        self.sheet = gs.open_by_key(sheet_id)

    IGNORE_GROUPS = ['test']

    def _make_repo_link(self, user_info):
        repo_link = gitlab_repos.url_for_student_repo(user_info)
        return '=HYPERLINK("{}";"git")'.format(repo_link)

    def _filter_users(self, users, group):
        return list(filter(lambda user: user.groupname == group, users))

    def _sort_users(self, users):
        user_key = lambda user: (user.groupname, user.lastname)
        return list(sorted(users, key=user_key))

    # NB: rows and columns are indexed from 1
    def _make_row_cells(self, row, column, count):
        cells = []
        for i in range(column, column + count):
            cells.append(gspread.Cell(row, i))
        return cells

    def export_to_gdoc(self, group, deadlines, scores_db):
        ws = self.sheet.worksheet(group)

        tasks = deadlines.task_names
        all_users = self._sort_users(self._filter_users(scores_db.list_users(), group))

        # Resize table

        ws.resize(len(all_users) + 1, len(tasks) + 7)

        # Cell updates

        updates = []

        # Headers

        header_cells = self._make_row_cells(row=1, column=1, count=7)

        header_cells[0].value = 'Group'
        header_cells[1].value = 'Last Name'
        header_cells[2].value = 'First Name'
        header_cells[3].value = 'Gitlab Username'
        header_cells[4].value = 'Project Url'
        header_cells[5].value = '%'
        header_cells[6].value = 'Total Score'

        updates.extend(header_cells)

        # Task Headers

        task_ordering = {}

        task_header_cells = self._make_row_cells(row=1, column=8, count=len(tasks))

        tasks = sorted(tasks, reverse=True)

        for task_index, task_name in enumerate(tasks):
            task_ordering[task_name] = task_index
            task_header_cells[task_index].value = task_name

        updates.extend(task_header_cells)

        # Users

        for idx, user_info in enumerate(all_users):
            user_row_index = 2 + idx

            # User info

            user_info_cells = self._make_row_cells(user_row_index, column=1, count=5)

            user_info_cells[0].value = user_info.groupname
            user_info_cells[1].value = user_info.lastname.capitalize()
            user_info_cells[2].value = user_info.firstname.capitalize()
            user_info_cells[3].value = user_info.username
            user_info_cells[4].value = self._make_repo_link(user_info)

            updates.extend(user_info_cells)

            # Task scores

            user_scores_cells = self._make_row_cells(user_row_index, column=8, count=len(tasks))

            user_scores = scores_db.get_user_scores(user_info.username)

            for task_index in range(len(tasks)):
                user_scores_cells[task_index].value = ''

            for task_name in user_scores:
                task_score = user_scores[task_name]
                task_index = task_ordering[task_name]
                user_scores_cells[task_index].value = task_score

            updates.extend(user_scores_cells)

        # Batch update

        ws.update_cells(updates, 'USER_ENTERED')
