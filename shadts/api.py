import functools
from collections import OrderedDict

from flask import request, abort, render_template, session, redirect, url_for

from . import app

from .gdoc import RatingsTable
from .course import Deadlines, CourseRepo
from .database import CourseDatabase
from .login import requires_auth
from . import gitlab_repos


def requires_token(f):
    @functools.wraps(f)
    def decorated(*args, **kwargs):
        token = request.form.get("token", "")
        if token != app.config["TESTER_TOKEN"]:
            abort(403)

        return f(*args, **kwargs)
    return decorated

@app.route("/api/export_to_gdoc", methods=["POST"])
def export_to_gdoc():
    course_repo = CourseRepo.from_config()
    scores_db = CourseDatabase.from_config()
    rt = RatingsTable.from_config()

    group = request.args.get("group")

    deadlines = course_repo.fetch_deadlines(group)
    rt.export_to_gdoc(group, deadlines, scores_db)

    return "", 200

@app.route("/api/report", methods=["POST"])
@requires_token
def report():
    if "failed" in request.form:
        return "", 200

    user = gitlab_repos.make_gitlab_api().users.get(int(request.form["user_id"]))

    db = CourseDatabase.from_config()
    user_info = db.get_user_info(user.username)

    course_repo = CourseRepo.from_config()
    deadlines = course_repo.fetch_deadlines(user_info.groupname)
    task = deadlines.find_task(request.form["task"])

    score = task.compute_current_score()
    app.logger.info("/report on task {}, username '{}', score {}".format(task.name, user.username, score))

    db.update_score(user.username, task.name, score)

    return "", 200

@app.route("/deadlines")
def view_tasks():
    user_group = request.args.get("group")
    course_repo = CourseRepo.from_config()
    deadlines = course_repo.fetch_deadlines(user_group)

    return render_template("tasks.html",
            task_base_url=course_repo.tasks,
            deadlines=deadlines,
            task_scores={})


@app.route("/")
@requires_auth
def main_page():
    username = session["gitlab"]["username"]

    db = CourseDatabase.from_config()
    user_info = db.get_user_info(username)

    course_repo = CourseRepo.from_config()
    deadlines = course_repo.fetch_deadlines(user_info.groupname)

    user_task_scores = db.get_user_scores(username, text=True)

    return render_template("tasks.html",
                           task_base_url=course_repo.tasks,
                           deadlines=deadlines,
                           task_scores=user_task_scores)
