import datetime

class ScoreCalculator:
    def __init__(self, deadline,
                 fall_period=datetime.timedelta(days=7),
                 end_penalty=0.5):
        self.deadline = deadline
        self.fall_period = fall_period
        self.end_penalty = end_penalty

    def score(self, full_score, submit_time):
        # In time

        if submit_time <= self.deadline:
            return full_score

        # Too late

        if submit_time >= self.deadline + self.fall_period:
            return int(full_score * self.end_penalty)

        # Score decreases linearly

        late = submit_time - self.deadline

        fall_period_seconds = int(self.fall_period.total_seconds())
        late_seconds = int(late.total_seconds())

        score_loss_limit = int(full_score * (1 - self.end_penalty))

        score_penalty = (score_loss_limit * late_seconds) // fall_period_seconds

        return full_score - score_penalty
